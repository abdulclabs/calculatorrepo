//
//  ViewController.swift
//  Calculator
//
//  Created by Click Labs on 1/14/15.
//  Copyright (c) 2015 clabs. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //text field variable...
    @IBOutlet weak var displayResult: UITextField!
    
    //global Variable...
    var isTypingNumber = false
    var firstNumber = 0.0
    var secondNumber = 0.0
    var operation = ""
    var isDotPressed = false
    var result = 0.0
    var iscontinueOperation = false
    
    // operators code...
    //plus button code...
    @IBAction func plushButton(sender: AnyObject) {
        
        isDotPressed = false
        
        operation = "+"
        firstNumber  = (displayResult.text as NSString).doubleValue
        displayResult.text = ""
        
    }
    //minus button code...
    @IBAction func minuButton(sender: AnyObject) {
        isDotPressed = false
        
        operation = "-"
        firstNumber = (displayResult.text as NSString).doubleValue
        displayResult.text = ""

    }
    
    //multyply button code...
    @IBAction func multiplyButton(sender: AnyObject) {
        
        isDotPressed = false
        
        operation = "*"
        firstNumber = (displayResult.text as NSString).doubleValue
        displayResult.text = ""
        
    }
    
    //divide button code...
    @IBAction func divideButton(sender: AnyObject) {
        
        isDotPressed = false
        
        operation = "/"
        firstNumber = (displayResult.text as NSString).doubleValue
        displayResult.text = ""
    }
    
    
    //numpad code...
    //dot button code...
    @IBAction func dotButton(sender: AnyObject) {
       
        if isDotPressed == false{
        var number = "."
        
        if isTypingNumber {
            displayResult.text = displayResult.text + number
        } else {
            displayResult.text = number
            isTypingNumber = true
        }
        }
        isDotPressed = true
    }
    
    //0 button code...
    @IBAction func digit0(sender: AnyObject) {
        
        var number = "0"
        
        if isTypingNumber {
            displayResult.text = displayResult.text + number
        } else {
            displayResult.text = number
            isTypingNumber = true
        }
    }
    //1 button code...
    @IBAction func digit1(sender: AnyObject) {
       
        var number = "1"
        
        if isTypingNumber {
            displayResult.text = displayResult.text + number
        } else {
            displayResult.text = number
            isTypingNumber = true
        }
    }
    
    //2 button code...
    @IBAction func digit2(sender: AnyObject) {
        var number = "2"
        
        if isTypingNumber {
            displayResult.text = displayResult.text + number
        } else {
            displayResult.text = number
            isTypingNumber = true
        }
    }
    
    //3 button code...
    @IBAction func digit3(sender: AnyObject) {
        
        var number = "3"
        
        if isTypingNumber {
            displayResult.text = displayResult.text + number
        } else {
            displayResult.text = number
            isTypingNumber = true
        }
    }
    //4 button code...
    @IBAction func digit4(sender: AnyObject) {
        var number = "4"
        
        if isTypingNumber {
            displayResult.text = displayResult.text + number
        } else {
            displayResult.text = number
            isTypingNumber = true
        }
    }
    //5 button code...
    @IBAction func digit5(sender: AnyObject) {
        var number = "5"
        
        if isTypingNumber {
            displayResult.text = displayResult.text + number
        } else {
            displayResult.text = number
            isTypingNumber = true
        }
    }
    
    //6 button code...
    @IBAction func digit6(sender: AnyObject) {
        
        var number = "6"
        
        if isTypingNumber {
            displayResult.text = displayResult.text + number
        } else {
            displayResult.text = number
            isTypingNumber = true
        }
    }
    
    //7 button code...
    @IBAction func digit7(sender: AnyObject) {
        
        var number = "7"
        
        if isTypingNumber {
            displayResult.text = displayResult.text + number
        } else {
            displayResult.text = number
            isTypingNumber = true
        }
    }
    //8 button code...
    @IBAction func digit8(sender: AnyObject) {
        
        var number = "8"
        
        if isTypingNumber {
            displayResult.text = displayResult.text + number
        } else {
            displayResult.text = number
            isTypingNumber = true
        }
    }
    
    //9 button code...
    @IBAction func digit9(sender: AnyObject) {
        
        var number = "9"
        
        if isTypingNumber {
            displayResult.text = displayResult.text + number
        } else {
            displayResult.text = number
            isTypingNumber = true
        }
    }
    
    //equal button code...
    @IBAction func equalButton(sender: AnyObject) {
        
        isTypingNumber = false
        isDotPressed = false
        
        //assign the second number...
        secondNumber = (displayResult.text as NSString).doubleValue
        
        //when plus button pressed...
        if operation == "+" {
            
            result = firstNumber + secondNumber
            displayResult.text = "\(result)"
            
        }
        //when minus button pressed...
        else if operation == "-" {
            
            result = firstNumber - secondNumber
            displayResult.text = "\(result)"
            
        }
        //when * button pressed...
        else if operation == "*" {
            
            result = firstNumber * secondNumber
            displayResult.text = "\(result)"
            
        }
        //when / button pressed...
        else if operation == "/" {
            
            //chek if check denominator is greater 
            if secondNumber.isZero == false {
            
                //when denominato is not zero then...
                var result = firstNumber / secondNumber
                displayResult.text = "\(result)"
                
            }
            else{
                //if denominator is zero error occured...
                displayResult.text = "Invalid Input!"
            }
            
        }else
        {
        
        }
        

    }
    
    
    //DEL button...
    
    @IBAction func delButton(sender: AnyObject) {
        
        var arr = Array(displayResult.text)
        if arr.count > 0 {
            
            //convert String to array...
            
            arr.removeLast()
            
            //convert array to String...
            var str = String(arr)
            displayResult.text = str
        } else {
            displayResult.text = ""
        }
        
        
        
    }
    // clear button to set the text field to nil...
    @IBAction func clearButton(sender: AnyObject) {
        
        isDotPressed = false
        var bool = displayResult.text
        if bool != nil {
        
        displayResult.text = nil
        
        }
    }
    
    //hide the keyboard when touches outside of the keyboard...
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

